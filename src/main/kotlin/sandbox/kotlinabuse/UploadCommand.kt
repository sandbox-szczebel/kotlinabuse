package sandbox.kotlinabuse

import picocli.CommandLine.Command
import picocli.CommandLine.Parameters
import java.nio.file.Path


@Command(name = "upload", description = ["- uploads files to S3"], mixinStandardHelpOptions = true)
class UploadCommand : Runnable {

    @Parameters(arity = "1", paramLabel = "FILE")
    lateinit var file: Path

    override fun run() = file uploadTo awsConfig.filesBucket

    infix fun Path.uploadTo(bucket: String) {
        println("uploading $this to $bucket")
        s3().uploadTo(bucket, this.toFile())
    }
}



