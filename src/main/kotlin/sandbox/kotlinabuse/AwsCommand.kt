package sandbox.kotlinabuse

import com.amazonaws.client.builder.AwsClientBuilder
import com.google.gson.GsonBuilder
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.containers.localstack.LocalStackContainer.Service.S3
import org.testcontainers.containers.localstack.LocalStackContainer.Service.SQS
import org.testcontainers.utility.DockerImageName
import picocli.CommandLine.Command
import java.net.URI

@Command(name = "aws", description = ["- manages aws services"], mixinStandardHelpOptions = true)
class AwsCommand {

    val network = Network.newNetwork()
    val localstack = LocalStackContainer(DockerImageName.parse("localstack/localstack:0.12.19.1"))
        .withNetwork(network)
        .withEnv("HOSTNAME_EXTERNAL", "localhost")
        .withNetworkAliases("localstack")
        .withServices(SQS, S3)

    var sqsMonitor = KGenericContainer(DockerImageName.parse("szczebel/sqs-monitor"))
        .withNetwork(network)
        .withEnv("AWS_REGION", localstack.region)
        .withEnv("QUEUE_URL_LOCALHOST_REPLACEMENT", "localstack")
        .withExposedPorts(8080)

    @Command(description = ["- spins up aws services"], mixinStandardHelpOptions = true)
    fun up() {
        println("Starting...")
        localstack.start()
        localstack saveTo awsConfig
        println(GsonBuilder().setPrettyPrinting().create().toJson(awsConfig))
        s3().createBucket(awsConfig.filesBucket)
        sqsMonitor.start()
        desktop browse "http://${sqsMonitor.host}:${sqsMonitor.firstMappedPort}"
    }
}

private fun AwsClientBuilder.EndpointConfiguration.url() = URI.create(serviceEndpoint).toURL()
private infix fun LocalStackContainer.saveTo(config : AwsConfig) {
    config.let {
        it.accessKey = accessKey
        it.secretKey = secretKey
        it.region = region
        it.sqsUrl = getEndpointConfiguration(SQS).url()
        it.s3Url = getEndpointConfiguration(S3).url()
        it.quotationQueueUrl = sqs().createQueue(it.quotationQueue).queueUrl
    }
}

class KGenericContainer(image:DockerImageName) : GenericContainer<KGenericContainer>(image)

