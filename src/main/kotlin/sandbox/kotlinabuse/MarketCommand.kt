package sandbox.kotlinabuse

import com.google.gson.Gson
import kotlinx.coroutines.DelicateCoroutinesApi
import picocli.CommandLine.Command
import picocli.CommandLine.Option

@Command(name = "market", description = ["- opens/closes stock market"], mixinStandardHelpOptions = true)
class MarketCommand {

    var stockMarket: StockMarket? = null

    @DelicateCoroutinesApi
    @Command(description = ["- starts producing quotations"], footer = ["\nExample : market open symbols=ZEPZ,GOOG,AMZN,FB"])
    fun open(@Option(names = ["symbols"], defaultValue = "ZEPZ,GOOG,AMZN,FB") symbols: String) {
        if(stockMarket != null) throw IllegalStateException("market already open")
        stockMarket = StockMarket(symbols.split(",")) { it.publish() }
        stockMarket!!.start()
    }

    @Command(description = ["- stops producing quotations"], mixinStandardHelpOptions = true)
    fun close() {
        stockMarket?.stop()
        stockMarket = null
    }
}

private fun Quotation.publish() = sqs().sendMessage(awsConfig.quotationQueueUrl, Gson().toJson(this))

