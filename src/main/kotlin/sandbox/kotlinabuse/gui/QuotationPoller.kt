package sandbox.kotlinabuse.gui

import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.google.gson.Gson
import kotlinx.coroutines.*
import sandbox.kotlinabuse.Quotation
import sandbox.kotlinabuse.awsConfig
import sandbox.kotlinabuse.sqs

class QuotationPoller(private val dispatcher: QuotationDispatcher) {

    private var pollingJob: Job? = null

    @DelicateCoroutinesApi
    fun start() {
        pollingJob = asyncLoop(100, sqs()) { pollAndDispatch(it) }
    }

    fun stop() = pollingJob?.cancel()

    private fun pollAndDispatch(sqs: AmazonSQS) {
        sqs
            .fetchMessages(awsConfig.quotationQueueUrl)
            .forEach {
                dispatcher.dispatch(Gson().fromJson(it.body, Quotation::class.java))
                sqs.deleteMessage(awsConfig.quotationQueueUrl, it.receiptHandle)
            }
    }
}

@DelicateCoroutinesApi
fun asyncLoop(delay: Long, sqs: AmazonSQS, function: suspend (AmazonSQS) -> Unit) : Job {
    return GlobalScope.launch {
        while (true) {
            delay(delay)
            function.invoke(sqs)
        }
    }
}

fun AmazonSQS.fetchMessages(queueUrl: String) =
    receiveMessage(ReceiveMessageRequest(queueUrl)
        .withMaxNumberOfMessages(10))
        .messages