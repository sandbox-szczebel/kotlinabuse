package sandbox.kotlinabuse.gui

import sandbox.kotlinabuse.Quotation
import java.util.*

typealias QuotationConsumer = (Quotation) -> Unit
typealias FilterBuilder = (QuotationConsumer) -> QuotationConsumer

interface QuotationSource {
    fun symbols() : Set<String>
    fun subscribe(o: QuotationConsumer): QuotationConsumer
    fun unsubscribe(o: QuotationConsumer)

    fun subscribe(symbol: String, subscriber: QuotationConsumer) : QuotationConsumer {
        return subscribe(filter(symbol)(subscriber))
    }

    companion object {
        fun filter(symbol: String): FilterBuilder = {
             { if(symbol == it.symbol) it(it) }
        }
    }
}

class QuotationDispatcher : QuotationSource {
    private val symbols: MutableSet<String> = HashSet()
    private val listeners: MutableList<QuotationConsumer> = LinkedList()

    override fun symbols(): Set<String> {
        return symbols
    }

    fun dispatch(q: Quotation) {
        symbols.add(q.symbol)
        //listeners.toList() prevents ConcurrentModification exception
        listeners.toList().forEach { it(q) }
    }

    override fun subscribe(o: QuotationConsumer) : QuotationConsumer {
        listeners.add(o)
        return o
    }

    override fun unsubscribe(o: QuotationConsumer) {
        listeners.remove(o)
    }
}

