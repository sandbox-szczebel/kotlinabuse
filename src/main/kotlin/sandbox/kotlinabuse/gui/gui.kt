package sandbox.kotlinabuse.gui

import io.javalin.Javalin
import kotlinx.coroutines.DelicateCoroutinesApi

class HttpServer {
    lateinit var javalin : Javalin
    val quotationDispatcher = QuotationDispatcher()
    val quotationPoller = QuotationPoller(quotationDispatcher)

    @DelicateCoroutinesApi
    infix fun startOn(port: Int) {
        quotationPoller.start()
        javalin = Javalin
            .create()
            .get("/",                           Pages.Static.HOME)
            .get("/files",                      Pages.Dynamic.Files)
            .get("/files/{filename}",           Pages.Dynamic.File)
            .get("/quotations",                 Pages.Dynamic.Quotations(quotationDispatcher))
            .get("/quotations/{symbol}",        Pages.Dynamic.QuotationPage)
            .sse("/quotations-stream/{symbol}", Pages.Dynamic.QuotationStream(quotationDispatcher))
            .start(port)
    }
    
    infix fun stop(unused: String) {
        quotationPoller.stop()
        javalin.stop()
    }
}

val now = "now!"

fun Javalin.get(path: String, html: String) = get(path){it.html(html)}
