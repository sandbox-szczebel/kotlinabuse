package sandbox.kotlinabuse.gui

import com.google.gson.Gson
import io.javalin.http.Handler
import io.javalin.http.sse.SseClient
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import sandbox.kotlinabuse.Quotation
import sandbox.kotlinabuse.s3
import java.util.function.Consumer

object Pages {

    object Static {
        val HOME = """
        <p>Ipsum lorem home page</p>
        <a href='files'>Files</a><br/>
        <a href='quotations'>Quotations</a><br/>
        """.trimIndent()
    }

    object Dynamic {
        val Files = Handler { ctx ->
            val fileLinks = s3()
                .listObjects("files-bucket")
                .objectSummaries
                .map { "<a href='files/${it.key}'>${it.key}</a><br/>" }
                .toTypedArray()
                .joinToString(separator = "\n")
            ctx.html(fileLinks)
        }

        val File = Handler { ctx ->
            val filename = ctx.pathParam("filename")
            val bytes = s3()
                .getObject("files-bucket", filename)
                .objectContent
                .readAllBytes()
            ctx.result(String(bytes))
        }

        fun Quotations(source : QuotationSource) : Handler = Handler { ctx ->
            val quotLinks = source
                .symbols()
                .map { "<a target='_blank' href='quotations/$it'>$it</a><br/>" }
                .toTypedArray()
                .joinToString(separator = "\n")
            ctx.html(quotLinks)
        }

        fun QuotationStream(source: QuotationSource) = Consumer<SseClient> { browser ->
            val symbol = browser.ctx.pathParam("symbol")
            val me = source.subscribe(symbol) { it sendTo browser}
            browser.onClose { source.unsubscribe(me)}
        }

        val QuotationPage = Handler { ctx ->
            val symbol = ctx.pathParam("symbol")
            ctx.html("""
                <body>
                    <p id="x">$symbol price:</p>
                    <div id="chart" style="height: 300px; width: 100%;"></div>
                </body>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                <script>
                    var prices = []
                    var chart = new CanvasJS.Chart("chart", {data: [{ type: "line", dataPoints: prices}]});
                    chart.render();

                    new EventSource("/quotations-stream/$symbol").onmessage = function(event) {
                      const q = JSON.parse(event.data)
                      document.getElementById("x").textContent = q.symbol + " price: " + q.price;
                      prices.push({y:q.price});
                      chart.render();
                    }
                </script>
                """.trimIndent())
        }
    }
}

private infix fun Quotation.sendTo(browser: SseClient) {
    //let's pretend that delivery of ZEPZ is slow (e.g. bandwidth issue)
    if("ZEPZ"==symbol) runBlocking { delay(2000) }
    browser.sendEvent(Gson().toJson(this))
}