package sandbox.kotlinabuse

import picocli.CommandLine
import picocli.CommandLine.Command
import kotlin.system.exitProcess

fun main() = App() loop { it.readEvalPrint() }

infix fun App.loop(function: (App) -> Unit) {
    while (true) function(this)
}

fun App.readEvalPrint() {
    print("\nkotlinabuse> ")
    execute(Console.readInput())
}

@Command(
    name = "kotlinabuse>",
    mixinStandardHelpOptions = true,
    version = ["ver.who.cares"],
    subcommands = [
        EchoCommand::class,
        GuiCommand::class,
        UploadCommand::class,
        AwsCommand::class,
        MarketCommand::class,
    ],
    footer = ["\nMost commands require a running AWS (localstack) services,", "so you should start by 'aws up'"]
)
class App {
    private val commandExecutor = CommandLine(this)


    init {
        execute(arrayOf("-h"))
    }

    fun execute(args: Array<String>) = commandExecutor.execute(*args)

    @Command(description = ["- exits the application"], mixinStandardHelpOptions = true)
    fun bye() {
        println("See ya later!")
        exitProcess(0)
    }
}


object Console {
    fun readInput() = readLine()!!.tokenize()

    private fun String.tokenize() = this
        .split(' ')
        .map { it.trim() }
        .filter { it.isNotBlank() }
        .toTypedArray()
}