package sandbox.kotlinabuse

import picocli.CommandLine.Command
import picocli.CommandLine.Parameters

@Command(name="echo", description = ["- echoes user input"], mixinStandardHelpOptions = true)
class EchoCommand : Runnable {

    @Parameters(paramLabel = "WORD", description = ["words to say"])
    lateinit var words: Array<String>

    override fun run() {
        println(words.joinToString(separator = " "))
    }
}