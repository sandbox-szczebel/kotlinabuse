package sandbox.kotlinabuse

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import java.io.File
import java.net.URI
import java.net.URL

class AwsConfig {
    val filesBucket: String = "files-bucket"
    val quotationQueue: String = "quotation-queue"

    var accessKey : String = "accesskey"
    var secretKey : String = "secretkey"
    var region : String = "us-east-1"
    var sqsUrl : URL = URI.create("http://localhost:4566").toURL()
    var s3Url : URL = URI.create("http://localhost:4566").toURL()
    var quotationQueueUrl: String = "no idea"

    fun creds() = AWSStaticCredentialsProvider(BasicAWSCredentials(accessKey, secretKey))
    fun URL.inCurrentRegion() = AwsClientBuilder.EndpointConfiguration(toString(), region)
    fun sqsConfig() = sqsUrl.inCurrentRegion()
    fun s3Config() = s3Url.inCurrentRegion()
}

val awsConfig = AwsConfig() //should be injected to all services instead, not a global var

fun sqs(): AmazonSQS {
    return AmazonSQSClientBuilder.standard()
        .withCredentials(awsConfig.creds())
        .withEndpointConfiguration(awsConfig.sqsConfig())
        .build() as AmazonSQS
}

fun s3(): AmazonS3Client {
    return AmazonS3ClientBuilder.standard()
        .withCredentials(awsConfig.creds())
        .withEndpointConfiguration(awsConfig.s3Config())
        .build() as AmazonS3Client
}

fun AmazonS3Client.uploadTo(bucket: String, payload: File) = putObject(bucket, payload.name, payload)


