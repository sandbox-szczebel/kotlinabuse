package sandbox.kotlinabuse

import kotlinx.coroutines.*
import kotlin.random.Random

class StockMarket (val symbols: List<String>, val sink: (Quotation) -> Unit) {

    val random = Random.Default
    var jobs : List<Job>? = null


    @DelicateCoroutinesApi
    fun start() {
        // Create an independent coroutine for each symbol
        // and store a reference to it, so that it can be stopped later.
        // Calling thread should return after starting the jobs

        GlobalScope.launch {
            jobs = symbols.map { launch { produceQuotationsFor(it) }}
        }
    }

    fun stop() {
        jobs?.forEach {it.cancel()}
    }

    suspend fun produceQuotationsFor(symbol: String) {
        var price = 1000
        while(true) {
            delay(500 + random.nextLong(-100, 100))
            val percentChange = random.nextInt(-3, 4)
            price += (percentChange*price)/100
            sink(Quotation(symbol, price))
        }
    }
}

data class Quotation(val symbol: String, val price: Int)