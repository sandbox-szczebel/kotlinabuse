package sandbox.kotlinabuse

import picocli.CommandLine.Command
import sandbox.kotlinabuse.gui.HttpServer
import sandbox.kotlinabuse.gui.now
import java.awt.Desktop
import java.awt.Desktop.getDesktop
import java.net.URI

@Command(name = "gui", description = ["- starts/stops http server"], mixinStandardHelpOptions = true)
class GuiCommand {

    val http = HttpServer()

    @Command(description = ["- starts http server"], mixinStandardHelpOptions = true)
    fun up() {
        http startOn 8080
        desktop browse "http://localhost:8080/"
    }

    @Command(description = ["- stops http server"], mixinStandardHelpOptions = true)
    fun down() = http stop now
}

val desktop = getDesktop()
infix fun Desktop.browse(url: String) = browse(URI.create(url))