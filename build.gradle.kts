plugins {
    kotlin("jvm") version "1.5.31"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation      ("info.picocli:picocli:4.6.1")
    implementation      ("io.javalin:javalin:4.1.1")
    implementation      ("org.slf4j:slf4j-simple:1.8.0-beta4")
    implementation      ("org.testcontainers:localstack:1.16.0")
    implementation      ("com.amazonaws:aws-java-sdk-sqs:1.12.88")
    implementation      ("com.amazonaws:aws-java-sdk-s3:1.12.88")
    implementation      ("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2")
    implementation      ("com.google.code.gson:gson:2.8.8")



//    implementation      ("software.amazon.awssdk:aws-sdk-java:2.17.60") //'commandline too long' error
//testcontainers.localstack not compatoble with AWS SDK v2 (java.lang.NoClassDefFoundError: com/amazonaws/auth/AWSCredentials on container init)
//    implementation      ("software.amazon.awssdk:auth:2.17.60")
//    implementation      ("software.amazon.awssdk:sqs:2.17.60")


    testImplementation  ("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly     ("org.junit.jupiter:junit-jupiter-engine")
}

tasks.test {
    useJUnitPlatform()
}
